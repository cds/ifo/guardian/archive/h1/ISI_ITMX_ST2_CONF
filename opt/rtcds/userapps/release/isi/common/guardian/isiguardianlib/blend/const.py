from .. import const as top_const

MAX_BLEND_NUMBER = 10

HPI_BLEND_FILTER_NAME = "BLND_{device}_{deg_of_free}"

BLEND_DEVICES_ALL = dict(
        BSC_ST1 = ['CPS','L4C','T240'],
        BSC_ST2 = ['CPS','GS13'],
        HAM = ['CPS','GS13'],
        HPI = ['IPS','L4C'],
    )


BLEND_FILTERS = {
    'A': {
        ('FM9'),
        },
    'B': {
        ('FM10'),
        },
    }

BLEND_CONSTANTS_ALL = dict(
        HAM = dict(
            LEVEL_FILTERS = {'A': BLEND_FILTERS['A']},
        ),
        BSC_ST1 = dict(
            LEVEL_FILTERS = {'A': BLEND_FILTERS['A']},
        ),
        BSC_ST2 = dict(
            LEVEL_FILTERS = {'A': BLEND_FILTERS['A']},
        ),
        HPI = dict(
            LEVEL_FILTERS = {'A': BLEND_FILTERS['A']},
        ),
    )


BLEND_CONSTANTS = BLEND_CONSTANTS_ALL[top_const.CHAMBER_TYPE]
BLEND_DEVICES = BLEND_DEVICES_ALL[top_const.CHAMBER_TYPE]



