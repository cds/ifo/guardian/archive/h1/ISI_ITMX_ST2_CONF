# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState
from .. import const as top_const
from ..ligoblend import LIGOBlendManager
import const as blend_const

def BLENDING_TIME(next_filter):
    """Times defined in seconds (LIGO-T1200126)"""
    if top_const.CHAMBER_TYPE == 'BSC_ST2':
        return 30
    else:
        return 60 if next_filter not in [9,10] else 120 

def get_basic_blend_switch_state(deg_of_free_list, blend_number, requestable=True):
    class BLEND_SWITCH_STATE(GuardState):
        request = requestable

        def main(self):
            self.lbm = LIGOBlendManager(deg_of_free_list=deg_of_free_list, ezca=ezca)
            self.lbm.transition_all_blends(blend_number, wait=False)
            self.timer['blending'] = BLENDING_TIME(blend_number)

        def run(self):
            # This timer is necessary because the blend filters are locked by the front-end code while
            # running. If we don't wait, then we will trigger a timeout by requesting for values on the
            # locked channels.
            if not self.timer['blending']:
                return False

            for deg_of_free in top_const.DOF_LIST:
                if self.lbm.is_transitioning_blends():
                    return False
            self.lbm.complete_blend_transition()
            return True

    return BLEND_SWITCH_STATE


def get_idle_blend_state(deg_of_free_list, blend_name, requestable=True):
    class BLEND_STATE_A(GuardState):
        request = requestable

        def main(self):
            return True
        def run(self):
            self.lbm = LIGOBlendManager(deg_of_free_list=deg_of_free_list, ezca=ezca)
            for deg_of_free in deg_of_free_list:
                if blend_name not in self.lbm.get_cur_blend_name(deg_of_free):
                    log('Blend %s is not in the correct config, going to DOWN'%(deg_of_free))
                    return 'DOWN'
                
            return True
            
    return BLEND_STATE_A

